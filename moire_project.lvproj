﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="exp_calculate.lvclass" Type="LVClass" URL="../expotional_calculate/exp_calculate.lvclass"/>
		<Item Name="gratings.lvclass" Type="LVClass" URL="../grating/gratings.lvclass"/>
		<Item Name="Talbot effect.lvclass" Type="LVClass" URL="../tablot effect/Talbot effect.lvclass"/>
		<Item Name="moire_main.vi" Type="VI" URL="../moire_main.vi"/>
		<Item Name="Verifications.lvclass" Type="LVClass" URL="../Verification/Verifications.lvclass"/>
		<Item Name="Moire_xy_subvi.vi" Type="VI" URL="../Moire_xy_subvi.vi"/>
		<Item Name="Moire-image-save_subvi.vi" Type="VI" URL="../Moire-image-save_subvi.vi"/>
		<Item Name="creat multi folder.vi" Type="VI" URL="/D/2019LabVIEW_code/建立資料夾/creat multi folder.vi"/>
		<Item Name="moire_0526.vi" Type="VI" URL="../moire_0526.vi"/>
		<Item Name="period analysis.vi" Type="VI" URL="../../period analysis.vi"/>
		<Item Name="Untitled 1.vi" Type="VI" URL="../../Untitled 1.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
				<Item Name="NI_AAL_SigProc.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AAL_SigProc.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="Moire_xshift_plus_array_subvi.vi" Type="VI" URL="../Moire_xshift_plus_array_subvi.vi"/>
			<Item Name="Moire_Rotated_plus_array_subvi.vi" Type="VI" URL="../Moire_Rotated_plus_array_subvi.vi"/>
			<Item Name="Talbot-image-save_subvi.vi" Type="VI" URL="../Talbot-image-save_subvi.vi"/>
			<Item Name="Talbot_xy_subvi.vi" Type="VI" URL="../Talbot_xy_subvi.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
