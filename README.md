*These files are part of the labview control software for the master degree of wafer alignment by using moire fringe in National Taiwan university .*

## Notice

In this program , we just simulate the morie fringes with very ideal circumstance according to diffraction formula.

If you want to know the optical simulation by Zemax or analyze by FFT,convoltion , you could refer [chunglee people github](https://github.com/chungleepeople/Moire-fringe-analysis) 

---

## Install
The code is used on LabVIEW 2015.

---

## What is **moire fringe** ?
In the semiconductor development process, wafer alignment is the great significance of the technology. 

During the wafer bonding or photolithography process, poor alignment causes the wafer to have defective products or even circuit discontinuities. 

Thus, the alignment of the wafers before photolithography is important. 

The Moiré fringes can be used for nano-level detection, which can be directly detected by the optical system through the magnification characteristics of the fringes without additional assistances.

[J. Zhu et al.](https://www.osapublishing.org/oe/abstract.cfm?uri=oe-21-3-3463)  designed a set of alignment marks, and a dual line grating which is composed of different periods side by side will have twice sensitivity of Moiré fringes

---

## Moire fringe diffracion Formula

Moiré fringe is a phenomenon that occurs in the superposition of two repetitive structures such as gratings.

This LabVIEW code is for reference  [**Fourier-based analysis of moiré fringe patterns of superposed gratings in alignment of nanolithography**](https://www.osapublishing.org/oe/abstract.cfm?uri=OE-16-11-7869)

1 
  [Phase difference caused by offset](https://ieeexplore.ieee.org/abstract/document/5270759)

2
  [Fringe deflection caused by rotated](https://www.osapublishing.org/oe/abstract.cfm?uri=OE-16-11-7869) 
  
---
